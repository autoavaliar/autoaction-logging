#!/usr/bin/make

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)
PWD := $(shell pwd)

default:
	echo ${CURRENT_UID}

.PHONY: composer
composer:
	docker run --rm --interactive --tty \
      --volume ${PWD}:/app \
      --user ${CURRENT_UID}:${CURRENT_GID} \
      composer install