<?php

declare(strict_types=1);

namespace AutoAction\Logging\Util;

class CamelCaseToSnakeCase
{
    public static function transform($value): string
    {
        return strtolower(
            preg_replace(
                ["/([A-Z]+)/", "/_([A-Z]+)([A-Z][a-z])/"],
                ["_$1", "_$1_$2"],
                lcfirst((string)$value)
            )
        );
    }
}