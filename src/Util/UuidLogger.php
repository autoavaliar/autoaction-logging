<?php

declare(strict_types=1);

namespace AutoAction\Logging\Util;

use Exception;

class UuidLogger
{
    private static $uuid;

    public static function uuid4(): string
    {
        if (!is_null(self::$uuid)) {
            return self::$uuid;
        }
        try {
            $data = random_bytes(16);
            assert(strlen($data) == 16);
            $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
            $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
            self::$uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
        } catch (Exception $e) {
            self::$uuid = 'error-when-generating';
        }
        return self::$uuid;
    }
}