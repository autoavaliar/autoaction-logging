<?php

declare(strict_types=1);

namespace AutoAction\Logging\Client;

use AutoAction\Logging\Config\BaseConfig;
use AutoAction\Logging\Config\LoggerLevelEnum;
use AutoAction\Logging\Util\CamelCaseToSnakeCase;
use AutoAction\Logging\Util\UuidLogger;
use Google\Cloud\Logging\Logger;
use Google\Cloud\Logging\LoggingClient;
use Google\Cloud\Logging\PsrLogger;

class LoggerAAV
{
    /** @var PsrLogger */
    private static $logging;
    private static $genericContext = [];
    private static $scope = 'undefined';

    public static function init(BaseConfig $config)
    {
        // https://cloud.google.com/logging/docs/agent/logging/configuration?hl=pt-br
        $logging = new LoggingClient([
            'projectId' => $config->getProjectId(),
            'keyFilePath' => $config->getKeyFilePath()
        ]);
        $name = str_replace(['/', '\\'], '-', ($config->getServiceName() . '-' . $config->getRouteName()));
        self::$logging = $logging->psrLogger($name, [
            'batchEnabled' => $config->isBatchEnabled(),
            'labels' => array_merge([
                'aav_environment' => $config->getEnvironment(),
                'aav_service' => $config->getServiceName(),
                'aav_scope' => self::$scope,
                'aav_service_route' => $config->getRouteName(),
                'aav_transaction_id' => UuidLogger::uuid4(),
            ], $config->getExtraLabels())
        ]);
    }

    public static function setScope(string $scope)
    {
        self::$scope = $scope;
    }

    public static function addGenericContext(string $key, $value)
    {
        self::$genericContext[$key] = CamelCaseToSnakeCase::transform($value);
    }

    private static function normalizeContext(array $context): array
    {
        return array_merge($context, self::$genericContext);
    }

    /**
     * Log an emergency entry.
     *
     * Example:
     * ```
     * $psrLogger->emergency('emergency message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function emergency(string $message, array $context = [])
    {
        self::$logging->log(Logger::EMERGENCY, $message, self::normalizeContext($context));
    }

    /**
     * Log an alert entry.
     *
     * Example:
     * ```
     * $psrLogger->alert('alert message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function alert(string $message, array $context = [])
    {
        self::$logging->log(Logger::ALERT, $message, self::normalizeContext($context));
    }

    /**
     * Log a critical entry.
     *
     * Example:
     * ```
     * $psrLogger->critical('critical message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function critical(string $message, array $context = [])
    {
        self::$logging->log(Logger::CRITICAL, $message, self::normalizeContext($context));
    }

    /**
     * Log an error entry.
     *
     * Example:
     * ```
     * $psrLogger->error('error message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function error(string $message, array $context = [])
    {
        self::$logging->log(Logger::ERROR, $message, self::normalizeContext($context));
    }

    /**
     * Log a warning entry.
     *
     * Example:
     * ```
     * $psrLogger->warning('warning message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function warning(string $message, array $context = [])
    {
        self::$logging->log(Logger::WARNING, $message, self::normalizeContext($context));
    }

    /**
     * Log a notice entry.
     *
     * Example:
     * ```
     * $psrLogger->notice('notice message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function notice(string $message, array $context = [])
    {
        self::$logging->log(Logger::NOTICE, $message, self::normalizeContext($context));
    }

    /**
     * Log an info entry.
     *
     * Example:
     * ```
     * $psrLogger->info('info message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function info(string $message, array $context = [])
    {
        self::$logging->log(Logger::INFO, $message, self::normalizeContext($context));
    }

    /**
     * Log a debug entry.
     *
     * Example:
     * ```
     * $psrLogger->debug('debug message');
     * ```
     *
     * @param string $message The message to log.
     * @param array  $context [optional] Please see {@see Google\Cloud\Logging\PsrLogger::log()}
     *                        for the available options.
     *
     * @return void
     */
    public static function debug(string $message, array $context = [])
    {
        self::$logging->log(Logger::DEBUG, $message, self::normalizeContext($context));
    }

    private static function setLevel(int $level): int
    {
        if (in_array($level, LoggerLevelEnum::validList())) {
            return $level;
        }
        return 100;
    }

    public static function exception(\Throwable $exception, int $level = Logger::DEBUG)
    {
        if ($exception->getCode() > 0 && $level == Logger::DEBUG) {
            $level = $exception->getCode();
        }
        $context = ['exception' => $exception];
        self::$logging->log(self::setLevel($level), $exception->getMessage(), self::normalizeContext($context));
    }
}