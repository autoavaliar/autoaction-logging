<?php

declare(strict_types=1);

namespace AutoAction\Logging\Config;

use AutoAction\Logging\Util\CamelCaseToSnakeCase;

abstract class BaseConfig
{
    protected $projectId = null;
    protected $keyFilePath = null;
    protected $environment = 'undefined';
    protected $serviceName = 'undefined';
    protected $routeName = 'undefined';
    protected $batchEnabled = false;
    protected $extraLabels = [];

    public function addExtraLabel(string $key, $value)
    {
        $key = 'aav_extra_' . CamelCaseToSnakeCase::transform($key);
        $this->extraLabels[$key] = (string)$value;
    }

    public function setProjectId(string $projectId): BaseConfig
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function setKeyFilePath(string $keyFilePath): BaseConfig
    {
        $this->keyFilePath = $keyFilePath;
        return $this;
    }

    public function setEnvironment(string $environment): BaseConfig
    {
        $this->environment = strtolower($environment);
        return $this;
    }

    public function setServiceName(string $serviceName): BaseConfig
    {
        $this->serviceName = $serviceName;
        return $this;
    }

    public function setRouteName(string $routeName): BaseConfig
    {
        $this->routeName = $routeName;
        return $this;
    }

    public function setBatchEnabled(bool $batchEnabled): BaseConfig
    {
        $this->batchEnabled = $batchEnabled;
        return $this;
    }

    public function setExtraLabels(array $extraLabels): BaseConfig
    {
        $this->extraLabels = $extraLabels;
        return $this;
    }

    public function getProjectId(): string
    {
        return $this->projectId;
    }

    public function getKeyFilePath()
    {
        return $this->keyFilePath;
    }

    public function getEnvironment(): string
    {
        return $this->environment;
    }

    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    public function getRouteName(): string
    {
        return $this->routeName;
    }

    public function isBatchEnabled(): bool
    {
        return $this->batchEnabled;
    }

    public function getExtraLabels(): array
    {
        return $this->extraLabels;
    }

}