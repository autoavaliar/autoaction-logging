<?php

declare(strict_types=1);

namespace AutoAction\Logging\Config;

class Generic extends BaseConfig
{
    public function __construct(string $projectId)
    {
        $this->projectId = $projectId;
    }
}