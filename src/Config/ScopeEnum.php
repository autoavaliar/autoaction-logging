<?php

declare(strict_types=1);

namespace AutoAction\Logging\Config;

class ScopeEnum
{
    const VALUATION = "valuation";
    const EGO = "ego";
}