<?php

declare(strict_types=1);

namespace AutoAction\Logging\Config;

class LoggerLevelEnum
{
    const EMERGENCY = 800;
    const ALERT = 700;
    const CRITICAL = 600;
    const ERROR = 500;
    const WARNING = 400;
    const NOTICE = 300;
    const INFO = 200;
    const DEBUG = 100;

    public static function validList(): array
    {
        return [
            self::EMERGENCY,
            self::ALERT,
            self::CRITICAL,
            self::ERROR,
            self::WARNING,
            self::NOTICE,
            self::INFO,
            self::DEBUG,
        ];
    }
}