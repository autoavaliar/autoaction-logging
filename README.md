# Exemplo de Uso

```php
<?php

use AutoAction\Logging\Client\LoggerAAV;
use AutoAction\Logging\Config\Generic;

require_once 'vendor/autoload.php';

// Configuração 
$config = new Generic('project-id-here');
$config->setBatchEnabled(true);
$config->setEnvironment(ENVIRONMENT);
$config->addExtraLabel('mvc_module',$this->dispatcher->getModuleName());
$config->addExtraLabel('mvc_controller',$this->dispatcher->getControllerName());
$config->addExtraLabel('mvc_action',$this->dispatcher->getActionName());
$config->addExtraLabel('instance_id',\SessionData::$instance_id);
$config->addExtraLabel('entity_id',\SessionData::$entity_id);
$config->addExtraLabel('user_id',\SessionData::$user_id);
$config->addExtraLabel('country_id',\SessionData::$country_id);
$config->setRouteName('route-full-name');
$config->setServiceName('my-system-name');

LoggerAAV::init($config);
LoggerAAV::addGenericContext('valuation_id', 12354);
```

# Detalhes de utilização

Visto que o pacote de logs está instalado no sistema, você só precisa chamar o `LoggerAAV::info|error|warning|alert|emergency|notice('Descrição do log',['context'=>'here']);`

Também é importante definir o contexto global, por exemplo antes de inicializar o controller definir `LoggerAAV::setScope(ScopeEnum::VALUATION);`

É possível também registrar uma exception `LoggerAAV::exception($e);`